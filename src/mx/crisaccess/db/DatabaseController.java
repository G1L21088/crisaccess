package mx.crisaccess.db;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import mx.crisaccess.model.DatabaseConnectModels;
import mx.crisaccess.model.DatabaseResponse;
import mx.crisaccess.model.User;

public class DatabaseController {

	private static interface DatabaseInterface {
		void result(ResultSet resultSet);
	}

	private static int doUpdateQuery(String query) {
		DatabaseHelper databaseHelper = new DatabaseHelper();
		int result = -1;
		try {
			Statement statement = databaseHelper.getConnection().createStatement();
			result = statement.executeUpdate(query);
			statement.close();
			databaseHelper.resignConnection();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
			return result;
		}
	}

	private static void doQuery(String query, DatabaseInterface databaseInterface) {
		DatabaseHelper databaseHelper = new DatabaseHelper();
		try {
			Statement statement = databaseHelper.getConnection().createStatement();
			databaseInterface.result(statement.executeQuery(query));
			statement.close();
			databaseHelper.resignConnection();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		}
	}

	private static void doQuery(String query, DatabaseInterface databaseInterface, int addressBase) {
		DatabaseHelper databaseHelper;
		DatabaseConnectModels model;
		switch (addressBase) {
		case 0:
			model = DatabaseConnectModels.getBaseCrisAcces();
			databaseHelper = new DatabaseHelper(model.get_IP(), model.get_PORT(), model.get_DB(), model.get_LOGIN(),
					model.get_PASSWORD());
			break;
		case 1:
			model = DatabaseConnectModels.getBasemydb();
			databaseHelper = new DatabaseHelper(model.get_IP(), model.get_PORT(), model.get_DB(), model.get_LOGIN(),
					model.get_PASSWORD());
			break;
		case 2:
			model = DatabaseConnectModels.getBasePruebaWf();
			databaseHelper = new DatabaseHelper(model.get_IP(), model.get_PORT(), model.get_DB(), model.get_LOGIN(),
					model.get_PASSWORD());
			break;
		default:
			model = DatabaseConnectModels.getBaseCrisAcces();
			databaseHelper = new DatabaseHelper(model.get_IP(), model.get_PORT(), model.get_DB(), model.get_LOGIN(),
					model.get_PASSWORD());
			break;
		}

		try {
			Statement statement = databaseHelper.getConnection().createStatement();
			databaseInterface.result(statement.executeQuery(query));
			statement.close();
			databaseHelper.resignConnection();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		}
	}

	private static void doQuery(String query, DatabaseInterface databaseInterface, String nameDB) {
		DatabaseHelper databaseHelper;
		DatabaseConnectModels model;
		model = DatabaseConnectModels.getBaseAny(nameDB);
		databaseHelper = new DatabaseHelper(model.get_IP(), model.get_PORT(), model.get_DB(), model.get_LOGIN(),
				model.get_PASSWORD());
		try {
			Statement statement = databaseHelper.getConnection().createStatement();
			databaseInterface.result(statement.executeQuery(query));
			statement.close();
			databaseHelper.resignConnection();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		}
	}

	public static DatabaseResponse newUser(User user) {
		DatabaseResponse databaseResponse = new DatabaseResponse();
		String query = "".concat("INSERT INTO").concat(" tb_users ").concat("(name, last_name, email, passwd)")
				.concat(" VALUES ").concat("('").concat(user.getName()).concat("', '").concat(user.getLatsName())
				.concat("', '").concat(user.getEmail()).concat("', '").concat(user.getPassword()).concat("');");
		System.out.println("QUERY: " + query);
		if (doUpdateQuery(query) == 1) {
			databaseResponse.setStatus(true);
			databaseResponse.setMsg("Insercción correcta");
		}
		return databaseResponse;
	}

	public static DatabaseResponse getUsers() {
		DatabaseResponse databaseResponse = new DatabaseResponse();
		ArrayList<User> arrayList = new ArrayList<>();
		String query = "".concat("SELECT * FROM ").concat(" tb_users ").concat(";");
		System.out.println("QUERY: " + query);
		doQuery(query, new DatabaseInterface() {
			@Override
			public void result(ResultSet resultSet) {
				try {
					while (resultSet.next()) {
						arrayList.add(new User(resultSet.getInt("id"), resultSet.getString("name"),
								resultSet.getString("last_name"), resultSet.getString("email"),
								resultSet.getString("passwd"), resultSet.getInt("status")));
					}
					databaseResponse.setStatus(true);
					databaseResponse.setMsg("Consulta Completa");
					databaseResponse.setData(arrayList);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println(e.getMessage());
					databaseResponse.setMsg("Error: " + e.getMessage());
				}
			}
		});
		return databaseResponse;
	}
	
	public static DatabaseResponse getUsers(String caso) {
		DatabaseResponse databaseResponse = new DatabaseResponse();
		ArrayList<User> arrayList = new ArrayList<>();
		String query = "".concat("SELECT * FROM ").concat(" tb_users ").concat(";");
		System.out.println("QUERY: " + query);
		doQuery(query, new DatabaseInterface() {
			@Override
			public void result(ResultSet resultSet) {
				try {
					while (resultSet.next()) {
						arrayList.add(new User(resultSet.getInt("id"), resultSet.getString("name"),
								resultSet.getString("last_name"), resultSet.getString("email"),
								resultSet.getString("passwd"), resultSet.getInt("status")));
					}
					databaseResponse.setStatus(true);
					databaseResponse.setMsg("Consulta Completa");
					databaseResponse.setData(arrayList);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println(e.getMessage());
					databaseResponse.setMsg("Error: " + e.getMessage());
				}
			}
		}, caso);
		return databaseResponse;
	}
	

	public static DatabaseResponse getUserbyEmail(String email) {
		DatabaseResponse databaseResponse = new DatabaseResponse();
		ArrayList<User> arrayList = new ArrayList<>();
		String query = "".concat("SELECT * FROM ").concat(" tb_users ").concat("WHERE").concat(" email='").concat(email)
				.concat("';");
		System.out.println("QUERY: " + query);
		doQuery(query, new DatabaseInterface() {
			@Override
			public void result(ResultSet resultSet) {
				try {
					while (resultSet.next()) {
						arrayList.add(new User(resultSet.getInt("id"), resultSet.getString("name"),
								resultSet.getString("last_name"), resultSet.getString("email"),
								resultSet.getString("passwd"), resultSet.getInt("status")));
					}
					if (arrayList.size() > 0) {
						databaseResponse.setStatus(true);
						databaseResponse.setMsg("Resultado de Usuario");
						databaseResponse.setData(arrayList.get(0));
					} else {
						databaseResponse.setStatus(false);
						databaseResponse.setMsg("No se encontraron elementos");
						databaseResponse.setData(null);
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println(e.getMessage());
					databaseResponse.setMsg("Error: " + e.getMessage());
				}
			}
		});
		return databaseResponse;
	}
}
