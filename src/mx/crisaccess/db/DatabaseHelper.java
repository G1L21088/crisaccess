package mx.crisaccess.db;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseHelper {

	private static final String _DB = "db_criss_access";
	private static final String _LOGIN = "root";
	private static final String _PASSWORD = "";
	private static final String _PORT = "3306";
	private static final String _DRIVER = "jdbc:mysql://";
	private static final String _IP = "127.0.0.1";
	private static final String _URL = _DRIVER + _IP + ":" + _PORT + "/" + _DB;
	private Connection connection = null;

	public DatabaseHelper() {
		try {
			Class.forName("org.gjt.mm.mysql.Driver");
			connection = DriverManager.getConnection(_URL, _LOGIN, _PASSWORD);
			if (connection != null)
				System.out.println("Conexi�n realizada a " + _DB);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		}
	}
	
	public DatabaseHelper(String _IP, String _PORT, String _DB, String _LOGIN, String _PASSWORD) {
		try {
			Class.forName("org.gjt.mm.mysql.Driver");
			connection = DriverManager.getConnection(_DRIVER.concat(_IP).concat(":").concat(_PORT).concat("/").concat(_DB), _LOGIN, _PASSWORD);
			if (connection != null)
				System.out.println("Conexi�n realizada a " + _DB);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		}
	}	

	public Connection getConnection() {
		return connection;
	}

	public void resignConnection() {
		connection = null;
	}

	public boolean isConnected() {
		return connection != null;
	}

}
