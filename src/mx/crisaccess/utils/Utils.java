package mx.crisaccess.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

	public static String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static String PHONE_PATTERN = "\\d{10}|(?:\\d{2}-){2}\\d{4}|\\(\\d{2}\\)\\d{4}-?\\d{4}";
	public static String INTERNATIONAL_PHONE_PATTERN = "^\\+[0-9]{1,3}\\.[0-9]{4,14}(?:x.+)?$";
	/// Minimum eight characters, at least one letter and one number
	public static String PASSWORD_PATTERN_A = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$";
	/// Minimum eight characters, at least one letter, one number and one special
	/// character:
	public static String PASSWORD_PATTERN_B = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$";
	/// Minimum eight characters, at least one uppercase letter, one lowercase
	/// letter and one number:
	public static String PASSWORD_PATTERN_C = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$";
	/// Minimum eight characters, at least one uppercase letter, one lowercase
	/// letter, one number and one special character:
	public static String PASSWORD_PATTERN_D = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}";
	/// Minimum eight and maximum 10 characters, at least one uppercase letter, one
	/// lowercase letter, one number and one special character:
	public static String PASSWORD_PATTERN_E = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,10}";

	public static boolean patternComparator(String s, String pattern) {
		Matcher matcher = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(s);
		return matcher.find();
	}

	public static boolean validObject(Object object) {
		return object != null;
	}

	public static String generateP(String s) {
		return "<p class=\"\">" + s + "</p>";
	}

	public static String generateA(String s) {
		return "<a class=\"\">" + s + "</a>";
	}

	public static String generateH1(String s) {
		return "<h1 class=\"\">" + s + "</h1>";
	}

	public static String generateTR(String[] s) {
		String tr = "";
		tr.concat("<tr>");
		for (String st : s) {
			tr.concat("<td>");
			tr.concat(st);
			tr.concat("</td>");
		}
		tr.concat("</tr>");
		return tr;
	}

}
