package mx.crisaccess.model;

public class DatabaseResponse {

	private boolean status;
	private String msg;
	private Object data;

	public DatabaseResponse() {
		super();
		this.status = false;
		this.msg = "";
		this.data = null;
	}

	public DatabaseResponse(boolean status, String msg, Object data) {
		super();
		this.status = status;
		this.msg = msg;
		this.data = data;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "DatabaseResponse [status=" + status + ", msg=" + msg + ", data=" + data + "]";
	}

}
