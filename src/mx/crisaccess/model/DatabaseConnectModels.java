package mx.crisaccess.model;

public class DatabaseConnectModels {

	private String _IP;
	private String _PORT;
	private String _DB;
	private String _LOGIN;
	private String _PASSWORD;

	public String get_IP() {
		return _IP;
	}

	public String get_PORT() {
		return _PORT;
	}

	public String get_DB() {
		return _DB;
	}

	public String get_LOGIN() {
		return _LOGIN;
	}

	public String get_PASSWORD() {
		return _PASSWORD;
	}

	public DatabaseConnectModels(String _IP, String _PORT, String _DB, String _LOGIN, String _PASSWORD) {
		super();
		this._IP = _IP;
		this._PORT = _PORT;
		this._DB = _DB;
		this._LOGIN = _LOGIN;
		this._PASSWORD = _PASSWORD;
	}

	public static DatabaseConnectModels getBaseCrisAcces() {
		return new DatabaseConnectModels("127.0.0.1", "3306", "db_criss_access", "root", "");
	}

	public static DatabaseConnectModels getBasePruebaWf() {
		return new DatabaseConnectModels("127.0.0.1", "3306", "pruebawf", "root", "");
	}

	public static DatabaseConnectModels getBasemydb() {
		return new DatabaseConnectModels("127.0.0.1", "3306", "mydb", "root", "");
	}
	
	public static DatabaseConnectModels getBaseAny(String name) {
		return new DatabaseConnectModels("127.0.0.1", "3306", name, "root", "");
	}


}
