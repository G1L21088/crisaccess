package mx.crisaccess.model;

public class User {
	
	private int id;
	private String name;
	private String latsName;
	private String email;
	private String password;
	private int status;
	
	public User(int id, String name, String latsName, String email, String password, int status) {
		super();
		this.id = id;
		this.name = name;
		this.latsName = latsName;
		this.email = email;
		this.password = password;
		this.status = status;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLatsName() {
		return latsName;
	}
	public void setLatsName(String latsName) {
		this.latsName = latsName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", latsName=" + latsName + ", email=" + email + ", password="
				+ password + ", status=" + status + "]";
	}
	
}
