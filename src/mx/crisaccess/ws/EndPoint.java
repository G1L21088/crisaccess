package mx.crisaccess.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import mx.crisaccess.db.DatabaseController;
import mx.crisaccess.model.DatabaseResponse;
import mx.crisaccess.model.User;

@Path("/servicio")
@Produces({MediaType.APPLICATION_JSON})
public class EndPoint {
	
	@GET
	@Path("/usuarios")
	public DatabaseResponse getUsuarios() {
		return DatabaseController.getUsers();
	}
	
	@GET
	@Path("/usuarios/{email}")
	public DatabaseResponse getUsuarios(@PathParam("email") String email) {
		return DatabaseController.getUserbyEmail(email);
	}
	
	@POST
	@Path("/usuarios")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public DatabaseResponse setUser(@FormParam("name") String name, @FormParam("last_name") String last_name, @FormParam("email") String email, @FormParam("password") String password) throws ApplicationException {
		DatabaseResponse databaseResponse = new DatabaseResponse();		
		databaseResponse = DatabaseController.newUser(new User(
				-1,
				name,
				last_name,
				email,
				password,
				-1
				));
		return databaseResponse;
	}
	
}

/**
 *  @GET
    @Path("/books")
    public Collection<Book> getBooks() {
    ...
    }
 
    @GET
    @Path("/book/{isbn}")
    public Book getBook(@PathParam("isbn") String id) {
    ...
    }
 
    @PUT
    @Path("/book/{isbn}")
    public Book addBook(@PathParam("isbn") String id, @QueryParam("title") String title) {
    ...
    }
 
    @POST
    @Path("/book/{isbn}")
    public Book updateBook(@PathParam("isbn") String id, String title) {
    ...
    }
 
    @DELETE
    @Path("/book/{isbn}")
    public Book removeBook(@PathParam("isbn") String id) {
    ...
    }
 */