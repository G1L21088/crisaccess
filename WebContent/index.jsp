<%@page import="java.util.ArrayList"%>
<%@page import="mx.crisaccess.model.DatabaseResponse"%>
<%@page import="mx.crisaccess.model.User"%>
<%@page import="mx.crisaccess.db.DatabaseController"%>
<%@page import="mx.crisaccess.utils.Utils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>- Cris Access -</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
    <!-- Theme style -->
    <link href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="${pageContext.request.contextPath}/resources/plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="${pageContext.request.contextPath}/resources/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="${pageContext.request.contextPath}/resources/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="${pageContext.request.contextPath}/resources/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="${pageContext.request.contextPath}/resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page">
	<div class="login-box">
		<div class="login-logo"><b>SERVICIO</b> SOCIAL</div>
		<div class="login-box-body">
			<p class="login-box-msg">Inicia sesión para administrar contenido</p>
			<form action="admin/login" method="post">
				<div class="form-group has-feedback">
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" placeholder="Contraseña" name="password">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
			</form>
		</div>		
	</div>
</body>
</html>

<%
//out.println("" + Utils.patternComparator("gilsantaella@gmail.com", Utils.EMAIL_PATTERN));
//DatabaseController.newUser(new User(-1, "Prueba 1", "Prueba 1", "Prueba 1", "Prueba 1", -1));
DatabaseResponse databaseResponse = DatabaseController.getUserbyEmail("gilsantaella@gmail.com");
	

User user = (User) databaseResponse.getData();

out.println(Utils.generateP(user.getEmail()));

user.getEmail();


out.println("\n\n\nSegunda petición\n\n\n");
	
	databaseResponse = DatabaseController.getUsers();

ArrayList<User> users = (ArrayList<User>) databaseResponse.getData();
for (User element : users)
	out.println(Utils.generateP(element.getEmail()));
	

/*String site = "http://www.facebook.com" ;
response.setStatus(response.SC_MOVED_TEMPORARILY);
response.setHeader("Location", site); */





%>